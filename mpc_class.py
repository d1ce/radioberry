import subprocess,os,stat,logging

logger = logging.getLogger(__name__)

MUSIC_PATH = "/var/lib/mpd/music/"
PLAYLIST_PATH = "/var/lib/mpd/playlists/"

def exec_cmd(cmd):
    result = ""
    c = cmd.split()
    p = subprocess.check_output(c)
    #print(cmd)
    for line in p.decode("utf8").split('\n'):
        result = result+line
    return result

class mpc:

    currentPlaylist = ""
    def __init__(self):
        pass

    def pl_prev(self):
        return exec_cmd("mpc prev")

    def pl_next(self):
        return exec_cmd("mpc next")

    def pl_clear(self):
        return exec_cmd("mpc clear")

    def pl_load(self,playlist):
        logger.debug('loading playlist %s',playlist)
        self.currentPlaylist = playlist
        return exec_cmd("mpc load " + playlist)

    def play(self, nr=0):
        lineCount = sum(1 for line in open(PLAYLIST_PATH + self.currentPlaylist +".m3u"))
        result = "No songs in playlist" + self.currentPlaylist
        if lineCount > 0:
            result = exec_cmd("mpc play %d" % nr)
        else:
            logger.warn(result)
        return result

    def repeat(self,on):
        if on:
            return exec_cmd("mpc repeat on")
        else:
            return exec_cmd("mpc repeat off")

    def current(self):
        return exec_cmd("mpc current")

    def getPlCount(self):
        p = subprocess.check_output(["mpc","playlist"])
        count = p.count('\n')
        return count

    def updateLib(self):
        logger.info('Updating playlist')
        playlist = open(PLAYLIST_PATH + "mp3.m3u",'w')
        found = False
        for root,dirs,files in os.walk(MUSIC_PATH,followlinks=True):
            for file in sorted(files):
                if (file.lower().endswith(".mp3")):
                    curFile = os.path.join(root,file)
                    logger.debug('found file: %s',curFile)
                    playlist.write(os.path.relpath(curFile,MUSIC_PATH)+"\n")
                    found = True
        playlist.close()
        os.chown(PLAYLIST_PATH + "mp3.m3u",45,45)
        os.chmod(PLAYLIST_PATH + "mp3.m3u", stat.S_IRGRP)
        logger.warn('No files found')
        exec_cmd("mpc update")
        return
