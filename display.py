from time import sleep
import sys,logging
import RPi.GPIO as GPIO

# Zuordnung der GPIO Pins
DISPLAY_RS = 15
DISPLAY_E  = 16
DISPLAY_DATA4 = 18
DISPLAY_DATA5 = 22
DISPLAY_DATA6 = 7
DISPLAY_DATA7 = 26

DISPLAY_WIDTH = 16      # Zeichen je Zeile
DISPLAY_LINE_0 = 0x80   # Adresse der ersten Display Zeile
DISPLAY_LINE_1 = 0xC0   # Adresse der zweiten Display Zeile
DISPLAY_CHR = True
DISPLAY_CMD = False
E_PULSE = 0.0005
E_DELAY = 0.0008


class logFilterBytes(object):
    def __init__(self,on):
        self.on=on
    def filter(self,logRecord):
        if (self.on) :
            return logRecord.name == 'bytes'
        else:
            return not logRecord.name == 'bytes'

logger = logging.getLogger(__name__)
byteLog = logging.getLogger("bytes")
byteHandler = logging.FileHandler('display_cmd.log',mode='w')
byteHandler.addFilter(logFilterBytes(False))
byteLog.addHandler(byteHandler)

def _rotate(string,n):
    return string[n:]+string[:n]

class display:
    def __init__(self,rotateSpeed):
        self.rotateSpeed = rotateSpeed
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(DISPLAY_E, GPIO.OUT)
        GPIO.setup(DISPLAY_RS, GPIO.OUT)
        GPIO.setup(DISPLAY_DATA4, GPIO.OUT)
        GPIO.setup(DISPLAY_DATA5, GPIO.OUT)
        GPIO.setup(DISPLAY_DATA6, GPIO.OUT)
        GPIO.setup(DISPLAY_DATA7, GPIO.OUT)
        GPIO.output(DISPLAY_E,0)
        GPIO.output(DISPLAY_RS,0)
        GPIO.output(DISPLAY_DATA4,0)
        GPIO.output(DISPLAY_DATA5,0)
        GPIO.output(DISPLAY_DATA6,0)
        GPIO.output(DISPLAY_DATA7,0)

        logger.debug('display GPIO setup DONE')
        self.display_init()


    def display_init(self):
        logger.debug('initalizing display')
        sleep(1)
        self.lcd_byte(0x00,DISPLAY_CMD)
        self.lcd_byte(0x33,DISPLAY_CMD)
        self.lcd_byte(0x32,DISPLAY_CMD)
        self.lcd_byte(0x28,DISPLAY_CMD)
        self.lcd_byte(0x0C,DISPLAY_CMD)
        self.lcd_byte(0x06,DISPLAY_CMD)
        self.lcd_byte(0x01,DISPLAY_CMD)


    def lcd_string(self,line,message):
        if line == 0:
            self.lcd_byte(DISPLAY_LINE_0, DISPLAY_CMD)
        elif line == 1:
            self.lcd_byte(DISPLAY_LINE_1, DISPLAY_CMD)
        else:
            return
        message = self.replace_special_chars(message)
        message = message.ljust(DISPLAY_WIDTH," ")
        for i in range(DISPLAY_WIDTH):
          self.lcd_byte(ord(message[i]),DISPLAY_CHR)


    def lcd_byte(self,bits, mode):
        byteLog.debug(bin(bits)[2:].zfill(8))
        GPIO.output(DISPLAY_RS, mode)
        GPIO.output(DISPLAY_DATA4, False)
        GPIO.output(DISPLAY_DATA5, False)
        GPIO.output(DISPLAY_DATA6, False)
        GPIO.output(DISPLAY_DATA7, False)
        if bits&0x10==0x10:
          GPIO.output(DISPLAY_DATA4, True)
        if bits&0x20==0x20:
          GPIO.output(DISPLAY_DATA5, True)
        if bits&0x40==0x40:
          GPIO.output(DISPLAY_DATA6, True)
        if bits&0x80==0x80:
          GPIO.output(DISPLAY_DATA7, True)
        sleep(E_DELAY)
        GPIO.output(DISPLAY_E, True)
        sleep(E_PULSE)
        GPIO.output(DISPLAY_E, False)
        sleep(E_DELAY)
        GPIO.output(DISPLAY_DATA4, False)
        GPIO.output(DISPLAY_DATA5, False)
        GPIO.output(DISPLAY_DATA6, False)
        GPIO.output(DISPLAY_DATA7, False)
        if bits&0x01==0x01:
          GPIO.output(DISPLAY_DATA4, True)
        if bits&0x02==0x02:
          GPIO.output(DISPLAY_DATA5, True)
        if bits&0x04==0x04:
          GPIO.output(DISPLAY_DATA6, True)
        if bits&0x08==0x08:
          GPIO.output(DISPLAY_DATA7, True)
        sleep(E_DELAY)
        GPIO.output(DISPLAY_E, True)
        sleep(E_PULSE)
        GPIO.output(DISPLAY_E, False)
        sleep(E_DELAY)

    def replace_special_chars(self,s):
        s = s.replace(chr(196), "Ae")
        s = s.replace(chr(214), "Oe")
        s = s.replace(chr(220), "Ue")

        s = s.replace(chr(228), chr(225))
        s = s.replace(chr(246), chr(239))
        s = s.replace(chr(252), chr(245))
        return s

    def println(self,line,text,interrupt):
        length = len(text)
        breakOut = False
        if length <= DISPLAY_WIDTH:
            self.lcd_string(line,text)
            breakOut = True

        if not breakOut:
            for i in range(0, length - DISPLAY_WIDTH +1 ):
                self.lcd_string(line,text[i:i+DISPLAY_WIDTH])
                if interrupt():
                    breakOut = True
                    break
                    sys.exit()
                sleep(self.rotateSpeed)
