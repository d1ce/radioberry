#!/usr/bin/python3
import RPi.GPIO as GPIO
from time import sleep
import atexit,logging,mpc_class,display


button_pressed = -1
interruptOn = False
current = ""
previous = ""


#GPIO ports
PREV_BUTTON = 11
NEXT_BUTTON = 12
MENU_BUTTON = 13


logging.basicConfig(level=logging.DEBUG,filemode='w',filename='radio.log',format='%(asctime)s:%(name)s:%(levelname)s:%(message)s',datefmt='%d.%m.%y %H:%M')

logger = logging.getLogger('radio')

logOutFormat = logging.Formatter('%(name)s:%(levelname)s:%(message)s')

logOutput = logging.StreamHandler()
logOutput.setLevel(logging.DEBUG)
logOutput.setFormatter(logOutFormat)


logging.getLogger('').addHandler(logOutput)

for handler in logging.root.handlers:
    handler.addFilter(display.logFilterBytes(False))

def switchstate(newState):
    global state,prevState
    global mainMode
    logger.debug('switching state: %d to %d',prevState,newState)
    if newState == 0:
        if prevState != 0:
            logger.info('Switched to internetradio')
            mpc.pl_clear()
            mpc.pl_load("radio")
            mpc.play()
    if newState == 1:
        if prevState != 1:
            logger.info('Switched to player')
            mpc.pl_clear()
            mpc.pl_load("mp3")
            mpc.play(1)
    if newState == 2:
        prevState = state
    state = newState
def checkTitle():
    global interruptOn,current,previous
    current = mpc.current()
    if previous != current:
        previous = current
        interruptOn = True
        logger.info('current: %s',current)
        if not current:
            current = "Playlist empty"
            logger.warn('mpc is not playing. This is most likely because of an empty playlist.')

    return

def getstate():
    global state
    return state

def radioMode():
    global button_pressed,previous,current,interruptOn

    if button_pressed == PREV_BUTTON:
        mpc.pl_prev()
        button_pressed = False
        sleep(0.5)

    if button_pressed == NEXT_BUTTON:
        mpc.pl_next()
        button_pressed = False
        sleep(0.5)

    if button_pressed == MENU_BUTTON:
        button_pressed = False
        switchstate(2)
        return


    checkTitle()

    display.println(0,"Internetradio",no_interrupt)
    display.println(1,current,interrupt)

def playerMode():
    global button_pressed,previous,current,interruptOn

    if button_pressed == PREV_BUTTON:
        mpc.pl_prev()
        button_pressed = False
        sleep(0.5)

    if button_pressed == NEXT_BUTTON:
        mpc.pl_next()
        button_pressed = False
        sleep(0.5)

    if button_pressed == MENU_BUTTON:
        button_pressed = False
        switchstate(2)
        return

    checkTitle()

    display.println(0,"Mp3-Player",no_interrupt)
    display.println(1,current,interrupt)

def displayMenu(m=-1):
    global menu,menuState
    display.println(0,"Menu - Mode",no_interrupt)
    if m != -1:
        menuState = m
    menu[menuState]()

def menuUp():
    s = menuState + 1
    l = len(menu)
    if s > l - 1:
        s -= l
    displayMenu(s)
    return

def menuDown():
    s = menuState - 1
    l = len(menu)
    if s < 0:
        s += l
    displayMenu(s)
    return

def menuCheckUpDown():
    global button_pressed
    if button_pressed == PREV_BUTTON:
        button_pressed = False
        menuDown()
        return

    if button_pressed == NEXT_BUTTON:
        button_pressed = False
        menuUp()
        return
    return

def menuRadio():
    global button_pressed
    display.println(1,"Internetradio",no_interrupt)
    menuCheckUpDown()
    if button_pressed == MENU_BUTTON:
        button_pressed = False
        switchstate(0)
        return


def menuPlayer():
    global button_pressed
    display.println(1,"Mp3-Player",no_interrupt)
    menuCheckUpDown()
    if button_pressed == MENU_BUTTON:
        button_pressed = False
        switchstate(1)
        return


def menuUpdate():
    global state,button_pressed,mpc
    display.println(1,"Update library",no_interrupt)
    menuCheckUpDown()
    if button_pressed == MENU_BUTTON:
        button_pressed = False
        mpc.updateLib()
        switchstate(prevState)
        return



#state machine
mainMode = {
    0:radioMode,
    1:playerMode,
    2:displayMenu,
    }

menu = {
    0:menuRadio,
    1:menuPlayer,
    2:menuUpdate,
    }
state = 0
prevState = 0
menuState = 0

def finish():
    logger.info('shutting down')
    mpc_class.exec_cmd("mpd --kill")
    logger.info('cleaning up GPIO')
    GPIO.cleanup()
    logger.info('done')

def button_callback(button):
    global button_pressed
    global interruptOn
    logger.debug('button pressed:%d',button)
    button_pressed = button
    interruptOn = True



def interrupt():
    global interruptOn,previous,current
    current = mpc.current()
    if previous != current:
        previous = current
        interruptOn = True

    if interruptOn:
        logger.debug('got interrupt')
        interruptOn = False
        return True
    else:
        return False

def no_interrupt():
    return False

def now_interrupt():
    return True



def main():
    global mpc
    global display
    mpc = mpc_class.mpc()
    display = display.display(0.5)
    try:
        logger.debug('initializing GPIO inputs')
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(PREV_BUTTON, GPIO.IN,pull_up_down=GPIO.PUD_UP)
        GPIO.setup(NEXT_BUTTON, GPIO.IN,pull_up_down=GPIO.PUD_UP)
        GPIO.setup(MENU_BUTTON, GPIO.IN,pull_up_down=GPIO.PUD_UP)

        GPIO.add_event_detect(PREV_BUTTON, GPIO.FALLING, callback = button_callback, bouncetime=200)
        GPIO.add_event_detect(NEXT_BUTTON, GPIO.FALLING, callback = button_callback, bouncetime=200)
        GPIO.add_event_detect(MENU_BUTTON, GPIO.FALLING, callback = button_callback, bouncetime=200)

        logger.debug('initializing GPIO inputs DONE')

        try:
            logger.debug('starting mpd')
            mpc_class.exec_cmd("mpd")
        except:
            logger.error('Failed to start mpd',exc_info=True)

        mpc_class.exec_cmd("mpc volume 100")
        mpc.pl_clear()
        logger.info('loading radio stations')
        mpc.pl_load("radio")
        mpc.repeat(True)
        mpc.play(1)
        logger.info('Radio started')
        while True:

            mainMode[state]()
            sleep(0.2)

        finish()
    except KeyboardInterrupt:
        logger.warn('Keyboard interrupt')
    finally:
        finish()

if __name__ == "__main__":
    main()
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
